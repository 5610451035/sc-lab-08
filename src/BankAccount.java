
public class BankAccount implements Measurable {
	
	private double balance;
	private String name;
	
	public BankAccount(String name,double balance){
		this.balance = balance;
		this.name = name;
	}
	
	public double getMeasure() {
		return balance;
	}
	
	public String toString() {
		return name;
	}
	public String getName(){
		return name;
	}
}
