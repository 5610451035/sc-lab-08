
public class Company implements Taxable{
	private String comName;
	private double income;
	private double expense;
	
	public Company(String comName,double income,double expense){
		this.comName = comName;
		this.income = income;
		this.expense = expense;
	}

	public double getTax() {
		return (income-expense)*0.3;
	}
	
	public String getComName(){
		return comName;
	}
}
