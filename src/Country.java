
public class Country implements Measurable {
	
	private int area;
	private String name;
	
	public Country(String name,int area){
		this.area = area;
		this.name = name;
	}
	public double getMeasure() {
		return area;
	}
	public String toString() {
		return name;
	}
	public String getName(){
		return name;
	}
	

}
