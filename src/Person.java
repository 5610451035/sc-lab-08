
public class Person implements Measurable {

	private String name;
	private int hight;
	
	public Person(String name,int hight){
		this.name = name;
		this.hight = hight;
	}
	
	public double getMeasure() {
		return hight;
	}
	public String toString(){
		return name;
	}
	public String getName(){
		return name;
	}

}
