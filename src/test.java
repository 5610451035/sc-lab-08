
import java.util.ArrayList;

public class test {
	public static void main(String[] args){
		Measurable[] list = new Measurable[4];
		Data data = new Data();
	
		Person p1 = new Person("Got",160);
		Person p2 = new Person("Bam",170);
		Person p3 = new Person("Boss",175);
		Person p4 = new Person("Nick",180);
		list[0] = p1;
		list[1] = p2;
		list[2] = p3;
		list[3] = p4;
		
		Country c1 = new Country("Singapore",20);
		Country c2 = new Country("Thailand",300);
		
		BankAccount b1 = new BankAccount("Somchai",5000);
		BankAccount b2 = new BankAccount("Somsuk",3500);
		
		System.out.println("Average of "+p1.getName()+","+p2.getName()+","+p3.getName()+","+p4.getName()+" : "+data.average(list));
		System.out.println("-------------------------------------\n");
		System.out.println("Min of "+p1.getName()+","+p2.getName()+" : "+data.min(p1, p2));
		System.out.println("Min of "+c1.getName()+","+c2.getName()+" : "+data.min(c1,c2));
		System.out.println("Min of "+b1.getName()+","+b2.getName()+" : "+data.min(b1,b2));
		System.out.println("-------------------------------------\n");
	
		
		
		ArrayList<Taxable> Perlist = new ArrayList<Taxable>();
		ArrayList<Taxable> Comlist = new ArrayList<Taxable>();
		ArrayList<Taxable> Prolist = new ArrayList<Taxable>();
		ArrayList<Taxable> PCPlist = new ArrayList<Taxable>();
		
		TaxCalculator tax = new TaxCalculator();
		
		PersonTax per1 = new PersonTax("Got",35000);
		PersonTax per2 = new PersonTax("Bam",27000);
		PersonTax per3 = new PersonTax("John",18000);
		
		Company com1 = new Company("AAA cop.",200000,80000);
		Company com2 = new Company("BBB cop.",300000,100000);
		Company com3 = new Company("CCC cop.",600000,150000);
		
		Product pro1 = new Product("mobile",12000);
		Product pro2 = new Product("computer",3500);
		Product pro3 = new Product("T.V.",75000);
		Product pro4 = new Product("clothing",5000);
		
		Perlist.add(per1);
		Perlist.add(per2);
		Perlist.add(per3);
		
		Comlist.add(com1);
		Comlist.add(com2);
		Comlist.add(com3);
		
		Prolist.add(pro1);
		Prolist.add(pro2);
		Prolist.add(pro3);
		
		PCPlist.add(per1);
		PCPlist.add(per2);
		PCPlist.add(per3);
		
		PCPlist.add(com1);
		PCPlist.add(com2);
		PCPlist.add(com3);
		
		PCPlist.add(pro1);
		PCPlist.add(pro2);
		PCPlist.add(pro3);
		PCPlist.add(pro4);
		
		System.out.println("Tax of "+per1.getName()+","+per2.getName()+","+per3.getName()+" : "+tax.sum(Perlist));
		System.out.println("Tax of "+com1.getComName()+","+com2.getComName()+","+com3.getComName()+" : "+tax.sum(Comlist));
		System.out.println("Tax of "+pro1.getProName()+","+pro2.getProName()+","+pro3.getProName()+" : "+tax.sum(Prolist));
		System.out.println("Sum tax of "+per1.getName()+","+per2.getName()+","+per3.getName()+","+com1.getComName()+","+com2.getComName()+","+com3.getComName()+","+pro1.getProName()+","+pro2.getProName()+","+pro3.getProName()+","+pro4.getProName()+" : "+tax.sum(PCPlist));
	

		
	}
	
 
}
